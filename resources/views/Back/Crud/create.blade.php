@extends('Back.layouts.master')

@section('title', trans('back.create-var',['var'=>trans('back.'.$model)]))

@section('style')
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/pages/editor_ckeditor.js') }}"></script>
@stop

@section('content')
    <x-page-header :model="$model" type="create"></x-page-header>

    <div class="panel panel-flat content">

        {!! Form::open(formCreateArray(str()->plural($model))) !!}

        <div class="panel-body">
            @include('Back.includes.flash')
            <div class="block block-rounded">
                <div class="block-header bg-smooth-dark col-md-10 col-md-offset-1">
                    @include('Back.'.ucfirst(str()->plural($model)).'.form')
                </div>
            </div>
        </div>

        <div class="panel-footer"><input type="submit" name="submit" class="btn btn-primary" value="@lang('back.save')"></div>

        {!! Form::close() !!}
    </div>
@stop
