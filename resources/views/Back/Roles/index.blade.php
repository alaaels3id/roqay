<x-model-index-page model="role" modelType="female">
    <table class="table datatable-basic" id="roles" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $key => $role)
            <tr id="role-row-{{ $role->id }}">
                <td>{{ $key+1 }}</td>

                <td>{{ $role->name ?? trans('back.no-value') }}</td>

                @include('Back.includes.changeModelStatus', ['status' => $role->status, 'id' => $role->id])

                <td>{{ $role->updated_at->diffForHumans() }}</td>

                <td>{{ $role->created_at->diffForHumans() }}</td>

                <td class="text-center">
                    @include('Back.includes.edit-delete', ['route' => 'roles', 'model' => $role])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
