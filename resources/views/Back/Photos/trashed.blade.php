@extends('Back.layouts.master')

@section('title', trans('back.trashed'))

@section('content')
    <x-page-header model="role" type="trashes"></x-page-header>

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        @include('includes.flash')

        <div class="panel-heading">
            <x-trashed-table-head table="photos" :collection="$trashes"></x-trashed-table-head>
        </div>

        <table class="table datatable-basic" id="photos" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>@lang('back.form-status')</th>
                <th>@lang('back.since')</th>
                <th>@lang('back.deleted_at')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($trashes as $key => $photo)
                <tr id="photo-row-{{ $photo->id }}">
                    <td>{{ $key+1 }}</td>

                    <td><img src="{{ $photo->image_url }}" width="90" height="90" alt=""></td>

                    <td>
                        @if($photo->status == 1)
                            <label class="label label-success">Active</label>
                        @else
                            <label class="label label-danger">Didactive</label>
                        @endif
                    </td>

                    <td>{{ $photo->created_at->diffForHumans() }}</td>

                    <td>{{ $photo->deleted_at->diffForHumans() }}</td>

                    <td class="text-center">
                        <x-trash-menu table="photos" :model="$photo"></x-trash-menu>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop
