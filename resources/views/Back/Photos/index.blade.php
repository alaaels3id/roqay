<x-model-index-page model="photo" modelType="female">
    <table class="table datatable-basic" id="photos" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-image')</th>
            <th>@lang('back.t-album')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($photos as $key => $photo)
            <tr id="photo-row-{{ $photo->id }}">
                <td>{{ $key+1 }}</td>

                <td><img src="{{ $photo->image_url }}" width="90" height="90" alt=""></td>

                <td><a href="{{ route('albums.show-album', $photo->album_id) }}">{{ $photo->album->name }}</a></td>

                @include('Back.includes.changeModelStatus', ['status' => $photo->status, 'id' => $photo->id])

                <td>{{ $photo->updated_at->diffForHumans() }}</td>

                <td>{{ $photo->created_at->diffForHumans() }}</td>

                <td class="text-center">
                    @include('Back.includes.edit-delete', ['route' => 'photos', 'model' => $photo])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
