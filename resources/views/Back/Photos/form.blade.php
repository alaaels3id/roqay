<div class="form-group">
    <x-select-input :arr="$albums" :errors="$errors" name="album_id" :slug="trans('back.t-album')"></x-select-input>
</div>

<div class="form-group">
    <x-form-inputs type="image" name="image" :slug="trans('back.form-image')" :errors="$errors"></x-form-inputs>

    <div class="col-xs-6">
        <div class="img-container">
            <img id="viewImage" class="img-responsive" width="90" height="90" src="{{ isset($currentModel) ? $currentModel->image_url : '' }}" alt=""/>
        </div>
    </div>
</div>

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
