<script>
    function isChecked(value, id) {
        if (value === 'checked')
        {
            let checkBoox = $('#checkbox-'+id);

            checkBoox.html('');

            checkBoox.html(getSwitchRender('true', id));

            $('#active-id-' + id).attr('onclick', 'isChecked("null", "' + id + '")');
        }
        else
        {
            let checkBoox = $('#checkbox-'+id);

            checkBoox.html('');

            checkBoox.html(getSwitchRender('false', id));

            $('#active-id-' + id).attr('onclick', 'isChecked("checked", "' + id + '")');
        }

        $.ajax({
            type: 'POST',
            url: '{{ route(str()->plural($model).'.ajax-change-'.$model.'-status') }}',
            data: {id, value},
            success: function (response) {
                new PNotify({
                    title: 'Success notice',
                    text: response.message,
                    addclass: 'bg-success ' + '{{ app()->getLocale() == 'en' ? 'stack-top-left' : 'stack-top-right' }}',
                    stack: {"dir1": "down", "dir2": "right", "push": "top"}
                });
            },
            error: (x) => crud_handle_server_errors(x),
        });
    }
</script>
