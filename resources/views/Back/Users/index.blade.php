<x-model-index-page model="user" modelType="male">
    <table class="table datatable-basic" id="users" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr id="user-row-{{ $user->id }}">
                <td>{{ $user->id }}</td>

                <td><a href="{{ route('users.show-user', $user->id) }}"> {{ $user->name ?? trans('back.no-value') }}</a></td>

                @include('Back.includes.changeModelStatus', ['status' => $user->status, 'id' => $user->id])

                <td>{{ $user->created_at->diffForHumans() }}</td>

                <td>{{ $user->updated_at->diffForHumans() }}</td>

                <td class="text-center">
                    @include('Back.includes.edit-delete', ['route' => 'users', 'model' => $user])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <x-slot name="scripts">
        <script>
            $('table#users').on('click', 'a.show-user-mail-sending-btn', function () {
                let clickedMessageAnchor = $(this);
                let ajaxUrl = clickedMessageAnchor.attr('href');
                let siteModal = $('div#site-modals');
                $.ajax({
                    type: 'GET',
                    url: ajaxUrl,
                    success: function (response) {
                        siteModal.html(response);
                        if (response.requestStatus && response.requestStatus === false) {
                            siteModal.html('');
                        } else {
                            $('#view_show_user_send_message').modal('show');
                        }
                    },
                    error: (x) => crud_handle_server_errors(x),
                });
                return false;
            });
        </script>
    </x-slot>
</x-model-index-page>
