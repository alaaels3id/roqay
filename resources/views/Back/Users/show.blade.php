@extends('Back.layouts.master')

@section('title', $user->name)

@section('content')
    <x-page-show-header model="user" :title="$user->name"></x-page-show-header>

    <div class="content">
        <div class="row">
            @include('includes.flash')
            <div class="col-md-8 col-md-offset-2">
                <!-- Basic table -->
                <div class="panel panel-primary">
                    <div class="panel-heading">@lang('back.user')</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3" style="float: {{ floating('right', 'left') }}">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img style="width: 300px;height: 200px;" src="{{ $user->image_url }}" alt="">
                                        <div class="caption-overflow">
                                            <span>
                                                <a href="{{ route('users.edit', $user->id) }}"
                                                   class="btn btn-info btn-sm">@lang('back.edit')</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="text-semibold no-margin-top" title="{{ $user->name }}">
                                            {{ ucwords(str()->limit($user->name, 30, '...')) }}
                                        </h6>
                                    </div>
                                </div>
                                <div class="lead">
                                    <button class="btn btn-success btn-block" data-toggle="modal" data-target="#view_show_user_send_notification">@lang('back.send-notification')
                                        <i class="icon-bell-check"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-9" style="float: {{ floating('left', 'right') }}">
                                <div class="well">
                                    <dl dir="{{ direction() }}">
                                        <dt>@lang('back.form-email')</dt>
                                        <dd>{{ $user->email ?? trans('back.no-value') }}</dd>

                                        <dt>@lang('back.form-status')</dt>
                                        <dd>
                                            @if($user->status == 1) <span
                                                class="label label-success">@lang('back.active')</span>
                                            @else <span class="label label-danger">@lang('back.disactive')</span>
                                            @endif
                                        </dd>

                                        <dt>@lang('back.since')</dt>
                                        <dd>{{ $user->created_at->diffForHumans() }}</dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic table -->
            </div>

            <div class="col-md-8 col-md-offset-2">
                <!-- Latest posts -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">@lang('back.albums')</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            @forelse($user->albums->chunk(2) as $chunks)
                                <div class="col-lg-6">
                                    <ul class="media-list content-group">
                                        @foreach($chunks as $album)
                                            <li class="media stack-media-on-mobile">
                                                <div class="media-left">
                                                    <div class="thumb">
                                                        <a href="{{ route('albums.show-album', $album->id) }}">
                                                            <img src="{{ $album->image_url ?? defaultImage() }}" class="img-responsive img-rounded media-preview" alt="">
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="media-body">
                                                    <h6 class="media-heading"><a href="{{ route('albums.show-album', $album->id) }}">{{ ucwords($album->name ?? trans('back.no-value')) }}</a></h6>
                                                    <ul class="list-inline list-inline-separate text-muted mb-5">
                                                        <li><i class="icon-microscope position-left"></i> {{ ucwords($user->name  ?? trans('back.no-value')) }}</li>
                                                        <li>{{ $album->created_at->diffForHumans() }}</li>
                                                    </ul>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @empty
                                <div class="alert alert-info text-center">@lang('back.no-value')</div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <!-- /latest posts -->
            </div>
        </div>
    </div>

    <div id="view_show_user_send_notification" class="modal fade">
        <div class="modal-dialog">
            <form action="{{ route('users.send-notification') }}" method="post">
                <div class="modal-content" dir="{{direction()}}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h1 class="modal-title">@lang('back.send-a-message')</h1>
                    </div>

                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="form-group @error('title') has-error @enderror">
                            <label for="title">@lang('back.form-title')</label>
                            <input type="text" name="title" id="title" class="form-control">
                            @error('title') <span
                                style="color: #d84315"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                        <div class="form-group @error('title') has-error @enderror">
                            <label for="body">@lang('back.form-body')</label>
                            <input type="text" name="body" id="body" class="form-control">
                            @error('body') <span style="color: #d84315"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">@lang('back.close')</button>
                        <button type="submit" name="submit" class="btn btn-primary">@lang('back.send')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
