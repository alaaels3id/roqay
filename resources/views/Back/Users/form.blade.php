<div class="form-group">
    <x-form-inputs type="text" name="name" :slug="trans('back.form-name')" :errors="$errors"></x-form-inputs>
</div>

<div class="form-group">
    <x-form-inputs type="email" name="email" :slug="trans('back.form-email')" :errors="$errors"></x-form-inputs>
</div>

@if(Route::is('users.create'))
	<div class="form-group">
        <x-form-inputs type="password" name="password" :slug="trans('back.form-password')" :errors="$errors"></x-form-inputs>
	</div>
	<div class="form-group">
        <x-form-inputs type="password_confirmation" name="password" :slug="trans('back.form-password')" :errors="$errors"></x-form-inputs>
	</div>
@endif

<div class="form-group">
    <x-form-inputs type="image" name="image" :slug="trans('back.form-image')" :errors="$errors"></x-form-inputs>
    <div class="col-xs-6">
		<div class="img-container">
			<img id="viewImage" class="img-responsive" width="90" height="90" src="{{ isset($currentModel) ? $currentModel->image_url : '' }}" alt=""/>
		</div>
	</div>
</div>

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>

@section('scripts')
    <script>$('input[name=image]').on('change', function(){readURL(this);});</script>
@stop
