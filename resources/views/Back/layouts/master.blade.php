<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ direction() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="http-root" content="{{ url(request()->root()) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.laravel = @json(['csrfToken' => csrf_token(),]) </script>

    <title>@lang('back.dashboard') || @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    @if(app()->isLocale('ar'))
    <link href="{{ asset('public/admin/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    @else
    <link href="{{ asset('public/admin/assets/css/components-ltr.css') }}" rel="stylesheet" type="text/css">
    @endif
    <link href="{{ asset('public/admin/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/004184547c.js" crossorigin="anonymous"></script>
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/pages/datatables_basic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/ckeditor/ckeditor.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/pages/form_input_groups.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/admin/assets/js/pages/form_layouts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/admin/assets/js/pages/components_notifications_pnotify.js') }}"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

@yield('style')
<!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
</head>
<body class="@yield('bodyclass')">

<div id="app">
    @if (!Route::is('admin.login'))
        <div class="navbar navbar-inverse bg-indigo">
            <div class="navbar-header navbar-{{ floating('left', 'right') }}">
                <a class="navbar-brand" style="float: {{ floating('right', 'left') }};"
                   href="{{ route('admin-panel') }}">
                    <img src="{{ asset('public/admin/assets/images/logo_light.png') }}" alt="">
                </a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav" style="float: {{ floating('right','left') }};">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>

                <div class="navbar-{{ floating('right','left') }}">
                    <ul class="nav navbar-nav" style="float: {{ floating('right','left') }};">
                        <li class="dropdown language-switch">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                @php
                                    $lang = app()->isLocale('ar') ? 'sa' : 'gb';
                                @endphp
                                <img src="{{ asset('public/admin/assets/images/flags/'.$lang.'.png') }}" alt="">
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ isLocalised("en") }}" class="english">
                                        <img src="{{ asset('public/admin/assets/images/flags/gb.png') }}" alt="">
                                        @lang('back.en')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ isLocalised("ar") }}" class="arabic">
                                        <img src="{{ asset('public/admin/assets/images/flags/sa.png') }}" alt="">
                                        @lang('back.ar')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    @auth('admin')
                        <p class="navbar-text" dir="{{ direction() }}">@lang('back.morning') {{ $auth->name }} !</p>
                        <p class="navbar-text"><span class="label bg-success-400">@lang('back.online')</span></p>
                    @endauth
                </div>
            </div>
        </div>
    @else
        <div class="navbar navbar-inverse bg-indigo">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('admin-panel') }}">
                    <img src="{{ asset('public/admin/assets/images/logo_light.png') }}" alt="">
                </a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                </ul>
            </div>
        </div>
    @endif

    <div class="page-container">
        <div class="page-content">
            @if (!Route::is('admin.login'))
                <div class="sidebar sidebar-main sidebar-default">
                    <div class="sidebar-content">
                        <div class="sidebar-user-material">
                            <div class="category-content">
                                <div class="sidebar-user-material-content">
                                    <a href="javascript:void(0);">
                                        <img src="{{ $auth->image_url }}" class="img-circle img-responsive" alt="">
                                    </a>
                                    <h6>{{ ucwords(isset($auth) ? $auth->name : 'UNAUTH') }}</h6>
                                    <span class="text-size-small">{{ isset($auth) ? $auth->email : 'UNAUTH' }}</span>
                                </div>

                                <div class="sidebar-user-material-menu">
                                    <a href="#user-nav" data-toggle="collapse"><span>@lang('back.my-account')</span> <i class="caret"></i></a>
                                </div>
                            </div>

                            <div class="navigation-wrapper collapse" id="user-nav">
                                <ul class="navigation">
                                    <li>
                                        <a href="{{ route('admins.profile') }}">
                                            <i class="icon-user-plus"></i>
                                            <span>@lang('back.my-account')</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form-1').submit();">
                                            <i class="icon-switch2"></i> @lang('back.logout')
                                        </a>

                                        <form id="logout-form-1" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <li class="navigation-header"><span>@lang('back.main')</span>
                                        <i class="icon-menu" title="Main pages"></i>
                                    </li>

                                    <li class="{{ setActive('admin-panel') }}">
                                        <a href="{{ route('admin-panel') }}"><i class="icon-home4"></i>
                                            <span>@lang('back.dashboard')</span>
                                        </a>
                                    </li>

                                    @if(permission_route_checker('contacts.index'))
                                        <li class="{{ setActive('admin-panel/contacts') }}">
                                            <a href="{{ route('contacts.index') }}">
                                                <i class="icon-paperplane"></i>
                                                <span>@lang('back.contacts')</span>
                                            </a>
                                        </li>
                                    @endif

                                    @foreach (models() as $model => $option)
                                        @php $models = str()->plural($model); @endphp

                                        @continue($models == 'settings' || $models == 'bankTransfers')

                                        @hasPermission($models.'.index')
                                            <li class="{{ setActive('admin-panel/'.$models.'/*') }}">
                                            <a href="javascript:void(0);">
                                                <i class="icon-{{ $option['icon'] }}"></i>
                                                <span>@lang('back.'.$models)</span>
                                            </a>

                                            <ul>
                                                @if(permission_route_checker($models.'.create'))
                                                    <x-menu-list-item :models="$models" type="create" trans="back.add"></x-menu-list-item>
                                                @endif

                                                @if(permission_route_checker($models.'.index'))
                                                    <x-menu-list-item :models="$models" type="" trans="back.all"></x-menu-list-item>
                                                @endif

                                                @if(permission_route_checker($models.'.trashed'))
                                                    <x-menu-list-item :models="$models" type="get/trashed" trans="back.trashed"></x-menu-list-item>
                                                @endif
                                            </ul>
                                        </li>
                                        @endhasPermission
                                    @endforeach

                                    @hasPermission('settings.index')
                                        <li class="{{ setActive('admin-panel/settings') }}">
                                            <a href="{{ route('settings.index') }}">
                                                <i class="icon-gear"></i>
                                                <span>@lang('back.settings')</span>
                                            </a>
                                        </li>
                                    @endhasPermission
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="content-wrapper">
                @yield('content')
                <div class="content">
                    <div class="footer text-muted text-center">&copy;{{ date('Y') }}. <a href="javascript:void(0);"> @lang('back.footer')</a></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="site-modals"></div>

<script type="text/javascript" src="{{ url('public/assets/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/assets/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/assets/js/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/crud.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/lang.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('public/assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery.printPage.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/scripts.js') }}"></script>

@yield('scripts')

</body>
</html>
