@extends('Back.layouts.master')

@section('title', trans('back.my-account'))

@section('style')
	<style>
		.admin-profile {
			background-image: url('{{url('http://demo.interface.club/limitless/assets/images/bg.png')}}');
			background-size: contain;
		}
		.admin-image { width: 110px; height: 110px; }
	</style>
@stop

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default" dir="{{ direction() }}">
        <div class="page-header-content">
           <div class="page-title">
               <x-page-title :title="trans('back.profile')"></x-page-title>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right', 'left') }}">
                <li><a href="{{ url('/admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.profile')</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

	<div class="content">
		<div class="row">
			<div class="col-md-3" style="float: {{ floating('right', 'left') }}">

				<div class="sidebar-detached">
					<div class="sidebar sidebar-default sidebar-separate">
						<div class="sidebar-content">
							<!-- User details -->
							<div class="content-group">
								<div class="panel-body bg-indigo-400 border-radius-top text-center admin-profile">
									<div class="content-group-sm">
										<h6 class="text-semibold no-margin-bottom">{{ ucwords($admin->name) }}</h6>
										<span class="display-block">{{ $admin->email }}</span>
									</div>

									<a href="javascript:void(0);" class="display-inline-block content-group-sm">
										<img src="{{ $admin->image_url }}" class="img-circle img-responsive admin-image" alt="">
									</a>
								</div>

								<div class="panel no-border-top no-border-radius-top">
									<ul class="navigation">
										<li class="navigation-header">@lang('back.main')</li>

										<li class="active">
											<a href="#profile" data-toggle="tab"><i class="icon-user"></i> @lang('back.profile')</a>
										</li>

										<li>
											<a href="#schedule" data-toggle="tab"><i class="icon-gear"></i> @lang('back.settings')</a>
										</li>

										<li class="navigation-divider"></li>

										<li>
	                                        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form-2').submit();">
	                                            <i class="icon-switch2"></i> @lang('back.logout')
	                                        </a>

	                                        <form id="logout-form-2" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
	                                            @csrf
	                                        </form>
										</li>
									</ul>
								</div>
							</div>
							<!-- /user details -->
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-9" style="float: {{ floating('left', 'right') }}">
				<div class="container-detached">

					<div class="content-detached">

						<!-- Tab content -->
						<div class="tab-content">
							<div class="tab-pane fade in active" id="profile">
								<!-- Profile info -->
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h6 class="panel-title">@lang('back.profile-info')</h6>
									</div>

									<div class="panel-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>@lang('back.form-name')</label>
													<input type="text" dir="{{ direction() }}" value="{{ ucwords($admin->name) }}" readonly class="form-control">
												</div>
												<div class="col-md-6">
													<label>@lang('back.form-email')</label>
													<input type="text" dir="{{ direction() }}" value="{{ $admin->email }}" readonly class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>@lang('back.form-phone')</label>
													<input type="text" dir="{{ direction() }}" value="{{ !empty($admin->phone) ? $admin->phone : trans('back.no-value') }}" readonly class="form-control">
												</div>
												<div class="col-md-6">
													<label>@lang('back.since')</label>
													<input type="text" dir="{{ direction() }}" value="{{ $admin->created_at->diffForHumans() }}" readonly class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>@lang('back.type')</label><br>
													@if ($admin->is_super_admin == 1)
														<label class="label label-success">@lang('back.super-admin')</label>
													@else
														<label class="label label-danger">@lang('back.admin')</label>
													@endif
												</div>
												<div class="col-md-6">
													<label>@lang('back.form-status')</label><br>
								                    @if($admin->status == 1) <span class="label label-success">@lang('back.active')</span>
								                    @else <span class="label label-danger">@lang('back.disactive')</span>
								                    @endif
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /profile info -->
							</div>

							<div class="tab-pane fade" id="schedule">
								<!-- Account settings -->
								<div class="panel panel-flat">

									<div class="panel-heading"><h6 class="panel-title">@lang('back.account-settings')</h6></div>

									<div class="panel-body">

								        {!!
								            Form::model($admin,[
								                'route'  => 'admins.admin-profile-update',
								                'method' => 'POST',
								                'class'  => 'admins ajax edit',
								                'files'  => true,
								            ])
								        !!}

										{!! Form::hidden('admin_id', $admin->id, ['class' => 'form-data']) !!}

										<div class="form-group">
											@include('Back.includes.inputs', [
												'errors' => $errors,
												'type'   => 'text',
												'name'   => 'name',
												'style'  => 'form-control form-data',
												'slug'   => trans('back.form-name'),
											])
										</div>

										<div class="form-group">
											@include('Back.includes.inputs', [
												'errors' => $errors,
												'type'   => 'text',
												'name'   => 'email',
												'style'  => 'form-control form-data',
												'slug'   => trans('back.form-email'),
											])
										</div>

										<div class="form-group">
											@include('Back.includes.inputs', [
												'errors' => $errors,
												'type'   => 'text',
												'name'   => 'phone',
												'attr' => '11',
												'style'  => 'form-control form-data',
												'slug'   => trans('back.form-phone'),
											])
										</div>

										@if (auth()->user()->role_id == 1)
										<div class="form-group">
											@include('Back.includes.inputs', [
												'errors' => $errors,
												'type'   => 'password',
												'name'   => 'password',
												'style'  => 'form-control form-data',
												'slug'   => trans('back.form-password'),
											])
										</div>

										<div class="form-group">
											@include('Back.includes.inputs', [
												'errors' => $errors,
												'type'   => 'password_confirmation',
												'name'   => 'password',
												'style'  => 'form-control form-data',
												'slug'   => trans('back.form-password-confirm'),
											])
										</div>
										@endif

										<div class="form-group">
											@include('Back.includes.inputs', [
												'errors' => $errors,
												'type'   => 'image',
												'name'   => 'image',
												'style'  => 'form-control form-data',
												'slug'   => trans('back.form-image'),
											])
										</div>

				                        <div class="text-right">
				                        	<input type="submit" name="submit" class="btn btn-primary" value="@lang('back.save')">
				                        </div>

					                    {!! Form::close() !!}
									</div>
								</div>
								<!-- /account settings -->
							</div>
						</div>
						<!-- /tab content -->
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
