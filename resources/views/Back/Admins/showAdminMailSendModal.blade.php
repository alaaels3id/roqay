<div id="view_show_admin_mail_sending" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" dir="{{direction()}}">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1 class="modal-title">@lang('back.send-a-message')</h1>
            </div>

            <div class="modal-body">
                <form action="{{ route('admins.send.message') }}" method="post" id="sendMessageForm">
                    @csrf
                    <input type="hidden" name="email" value="{{ $admin->email }}">
                    <div class="form-group">
                        <label for="message">@lang('back.t-contact')</label>
                        <textarea name="message" id="message" class="form-control" cols="4" style="resize: vertical;"></textarea>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <input type="submit" value="@lang('back.send')" class="btn btn-success pull-left" form="sendMessageForm">
                <button type="button" class="btn btn-link pull-right" data-dismiss="modal">@lang('back.close')</button>
            </div>
        </div>
    </div>
</div>
