@extends('Back.layouts.master')

@section('title', trans('back.trashed'))

@section('content')
    <x-page-header model="admin" type="trashes"></x-page-header>

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        @include('includes.flash')

        <div class="panel-heading">
            <x-trashed-table-head table="admins" :collection="$trashes"></x-trashed-table-head>
        </div>

        <table class="table datatable-basic" id="admins" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>@lang('back.form-image')</th>
                <th>@lang('back.form-status')</th>
                <th>@lang('back.since')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($trashes as $key => $admin)
                <tr id="admin-row-{{ $admin->id }}">
                    <td>{{ $key+1 }}</td>

                    <td>{{ $admin->name ?? trans('back.no-value') }}</td>

                    <td><img width="60" height="60" class="img-circle" src="{{ $admin->image_url }}" alt=""></td>

                    <td>
                        @if($admin->status == 1)
                            <label class="label label-success">Active</label>
                        @else
                            <label class="label label-danger">Didactive</label>
                        @endif
                    </td>

                    <td>{{ $admin->created_at->diffForHumans() }}</td>

                    <td class="text-center">
                        <x-trash-menu table="admins" :model="$admin"></x-trash-menu>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop
