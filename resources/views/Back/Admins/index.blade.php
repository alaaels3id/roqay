<x-model-index-page model="admin" modelType="male">
    <table class="table datatable-basic" id="admins" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-email')</th>
            <th>@lang('back.form-image')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($admins as $key => $admin)
            @if(auth()->user()->id != $admin->id)
                <tr id="admin-row-{{ $admin->id }}">
                    <td>{{ $key+1 }}</td>

                    <td>
                        <a href="{{ route('admins.show-admin', $admin->id) }}">{{ $admin->name ?? trans('back.no-value') }}</a>
                    </td>

                    <td>
                        <a
                            href='{{route('admins.show.message', $admin->id)}}'
                            class='btn btn-md btn-links show-admin-mail-sending-btn'
                            type='button'
                            data-toggle='tooltip'>{{ str()->limit($admin->email,30) }}
                        </a>
                    </td>

                    <td><img width="60" height="60" class="img-circle" src="{{ $admin->image_url }}" alt=""></td>

                    @include('Back.includes.changeModelStatus', ['status' => $admin->status, 'id' => $admin->id])

                    <td>{{ $admin->created_at->diffForHumans() }}</td>

                    <td class="text-center">
                        @include('Back.includes.edit-delete', ['route' => 'admins', 'model' => $admin])
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    <x-slot name="scripts">
        <script>
            $('table#admins').on('click', 'a.show-admin-mail-sending-btn', function () {
                let clickedMessageAnchor = $(this);
                let ajaxUrl = clickedMessageAnchor.attr('href');
                let siteModal = $('div#site-modals');
                $.ajax({
                    type: 'GET',
                    url: ajaxUrl,
                    success: function (response) {
                        siteModal.html(response);
                        if (response.requestStatus && response.requestStatus === false) {
                            siteModal.html('');
                        } else {
                            $('#view_show_admin_mail_sending').modal('show');
                        }
                    },
                    error: (x) => crud_handle_server_errors(x),
                });
                return false;
            });
        </script>
    </x-slot>
</x-model-index-page>
