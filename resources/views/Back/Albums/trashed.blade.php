@extends('Back.layouts.master')

@section('title', trans('back.trashed'))

@section('content')
    <x-page-header model="role" type="trashes"></x-page-header>

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        @include('includes.flash')

        <div class="panel-heading">
            <x-trashed-table-head table="albums" :collection="$trashes"></x-trashed-table-head>
        </div>

        <table class="table datatable-basic" id="albums" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>@lang('back.form-status')</th>
                <th>@lang('back.since')</th>
                <th>@lang('back.deleted_at')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($trashes as $key => $album)
                <tr id="album-row-{{ $album->id }}">
                    <td>{{ $key+1 }}</td>

                    <td>{{ $album->name ?? trans('back.no-value') }}</td>

                    <td>
                        @if($album->status == 1)
                            <label class="label label-success">Active</label>
                        @else
                            <label class="label label-danger">Didactive</label>
                        @endif
                    </td>

                    <td>{{ $album->created_at->diffForHumans() }}</td>

                    <td>{{ $album->deleted_at->diffForHumans() }}</td>

                    <td class="text-center">
                        <x-trash-menu table="albums" :model="$album"></x-trash-menu>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop
