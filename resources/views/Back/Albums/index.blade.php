<x-model-index-page model="album" modelType="male">
    <table class="table datatable-basic" id="albums" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($albums as $key => $album)
            <tr id="album-row-{{ $album->id }}">
                <td>{{ $key+1 }}</td>

                <td><a href="{{ route('albums.show-album', $album->id) }}"> {{ $album->name ?? trans('back.no-value') }}</a></td>

                @include('Back.includes.changeModelStatus', ['status' => $album->status, 'id' => $album->id])

                <td>{{ $album->updated_at->diffForHumans() }}</td>

                <td>{{ $album->created_at->diffForHumans() }}</td>

                <td class="text-center">
                    @include('Back.includes.edit-delete', ['route' => 'albums', 'model' => $album])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
