<div class="form-group">
    <x-form-inputs type="text" name="name" :slug="trans('back.form-name')" :errors="$errors"></x-form-inputs>
</div>

<div class="form-group">
    <x-select-input :arr="$users" :errors="$errors" name="user_id" :slug="trans('back.t-user')"></x-select-input>
</div>

<div class="form-group">
    <x-select-input :arr="$types" :errors="$errors" name="type" :slug="trans('back.type')"></x-select-input>
</div>

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
