@extends('Back.layouts.master')

@section('title', $album->name)

@section('content')
    <x-page-show-header model="album" :title="$album->name"></x-page-show-header>

    <div class="content">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">@lang('back.album')</div>
                    <div class="panel-body">
                    	<div class="row">
                    		<div class="col-md-3" style="float: {{ floating('right', 'left') }}">
                    			<div class="thumbnail">
    								<div class="thumb">
                                        <img style="width: 300px;height: 200px;" src="{{ $album->image_url }}" alt="">
    									<div class="caption-overflow">
    										<span>
    											<a href="{{ route('albums.edit', $album->id) }}" class="btn btn-info btn-sm">
                                                    @lang('back.edit')
                                                </a>
    										</span>
    									</div>
    								</div>

    								<div class="caption">
    									<h6 class="text-semibold no-margin-top" title="{{ $album->name }}">
    										{{ ucwords(str()->limit($album->name, 30, '...')) }}
    									</h6>
    								</div>
    							</div>
                    		</div>

                    		<div class="col-md-9" style="float: {{ floating('left', 'right') }}">
								<div class="well">
				                    <dl dir="{{ direction() }}">
										<dt>@lang('back.type')</dt>
										<dd>{{ trans('back.'.$album->type) ?? trans('back.no-value')  }}</dd>

										<dt>@lang('back.form-status')</dt>

										<dd>@include('Back.includes.is-active', ['status' => $album->status])</dd>

										<dt>@lang('back.since')</dt>
										<dd>{{ $album->created_at->diffForHumans() }}</dd>
									</dl>
								</div>
                    		</div>
                    	</div>
                    </div>
                </div>
    		</div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">@lang('back.photos')</div>
                    <div class="panel-body">
                        <div class="row">
                            @forelse($album->images as $image)
                                <div class="col-lg-4" id="photo-row-{{$image->id}}">
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h6 class="panel-title"></h6>
                                            <div class="heading-elements">
                                                <ul class="icons-list">
                                                    <li><a data-action="collapse"></a></li>
                                                    <li><a onclick="window.location.href='{{ route('photos.edit',$image->id) }}'"><i class="icon-pencil7"></i></a></li>
                                                    <li>
                                                        <a onclick="event.preventDefault();removeImage('{{ $image->id }}');">
                                                            <i class="icon-close2"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="panel-body">
                                            <div class="thumbnail">
                                                <img src="{{ $image->image_url }}" style="width:233px; height:193px;" alt=""><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="alert alert-info text-center">@lang('back.no-value')</div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        function removeImage(id)
        {
            swal({
                title: 'رسالة',
                text: 'هل انت متاكد من حذف هذه الصورة',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('photos.ajax-delete-photo') }}',
                            data: {id},
                            success: function (response) {
                                if (response.deleteStatus) {
                                    $(`#photo-row-${id}`).fadeOut();
                                    swal(response.message, {icon: "success"});
                                } else {
                                    swal(response.error);
                                }
                            },
                            error: (x) => crud_handle_server_errors(x),
                        });
                    } else {
                        swal({
                            title: "@lang('back.a-message')",
                            text: "تم إلغاء العملية",
                            icon: "success",
                            button: "حسنا",
                        });
                    }
                });
        }
    </script>
@stop
