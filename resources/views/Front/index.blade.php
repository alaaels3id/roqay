@extends('layouts.app')

@section('title', trans('back.home'))

@section('content')
    @include('includes.nav')
    <section class="check_demo_movie">
        <div class="container">
            <h2 class=" wow fadeInDown">Check Our <span class="main-color"> @lang('back.albums')</span></h2>
            <div class="row" style="margin: 5px;">
                @forelse($albums as $album)
                    <div class="col-md-4">
                        <div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                            <div class="card-header">
                                <img onclick="window.location.href='{{ route('front.show.album', $album->id) }}'" style="cursor: pointer;" src="{{ $album->image_url }}" class="lazyload" alt="">
                            </div>
                            <div class="card-body">
                                <h4><a href="#"> {{ ucwords($album->name) }}</a></h4>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12 alert alert-info text-center">@lang('back.no-value')</div>
                @endforelse
            </div>
        </div>
    </section>
@endsection
