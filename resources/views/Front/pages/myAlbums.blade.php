@extends('layouts.app')

@section('title', trans('back.albums'))

@section('content')
    @include('includes.nav')
    <section class="check_demo_movie">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="row" style="margin: 5px;">
                        @forelse($user->albums as $album)
                            <div class="col-md-6">
                                <div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                                    <div class="card-header">
                                        <img onclick="window.location.href='{{ route('front.single.album', $album->id) }}'" style="cursor: pointer;" src="{{ $album->image_url }}" class="lazyload" alt="">
                                    </div>
                                    <div class="card-body">
                                        <h4><a href="#"> {{ ucwords($album->name) }}</a></h4>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-md-12 alert alert-info text-center">@lang('back.no-value')</div>
                        @endforelse
                    </div>
                </div>
                <div class="col-md-3 contact-form">
                    @include('includes.flash')
                    <form action="{{ route('front.user.album.create') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">@lang('back.form-name')</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name">
                            @error('name') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                        <div class="form-group">
                            <label for="type">@lang('back.type')</label>
                            <select name="type" class="form-control @error('name') is-invalid @enderror" id="type">
                                <option value="" disabled selected>Select a value</option>
                                <option {{ old('type') == 'public' ? 'selected' : '' }} value="public">Public</option>
                                <option {{ old('type') == 'private' ? 'selected' : '' }} value="private">Private</option>
                            </select>
                            @error('type') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success btn-block" value="@lang('back.add')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
