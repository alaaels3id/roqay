@extends('layouts.app')

@section('title', trans('back.profile'))

@section('content')
    @include('includes.nav')
    <div class="container mt-5 mb-5">
        @include('includes.flash')
        <div class="row">
            <div class="col-md-3">
                <div class="text-center mt-3 pb-3 bg-light">
                    <div class="justify-content-center pb-3 pt-5">
                        <img src="{{ $user->image_url }}" class="img-fluid" style="border-radius: 50%;" width="100" height="100" alt="">
                    </div>

                    <h5>{{ ucwords($user->name) }}</h5>
                    <p class="title">{{ $user->email }}</p>

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-muted active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                            <div class="d-flex justify-content-start">
                                <h5 class="ml-3 text-white">@lang('back.personal-info')</h5>
                            </div>
                        </a>

                        <a class="nav-link text-muted" href="{{ route('user.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <div class="d-flex justify-content-start">
                                <h5 class="ml-3">@lang('back.logout')</h5>
                            </div>
                        </a>

                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="mt-3">
                    <div class="tab-content p-3" id="v-pills-tabContent">
                        <div class="tab-pane fade show active wow fadeInLeft" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">

                            <div class="d-flex justify-content-between pt-3 pb-3">
                                <h4 class="text-primary">@lang('back.personal-info')</h4>
                                <a href="#" data-toggle="modal" data-target="#exampleModalCenter">
                                    <i class="fas fa-edit text-primary"></i>
                                </a>
                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">@lang('back.personal-info-edit')</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ route('front.updateProfile') }}" class="container" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="image">Image</label>
                                                        <input type="file" name="image" id="image" class="form-control"/>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name">@lang('back.form-name')</label>
                                                        <input type="text" id="name" name="name" value="{{ $user->name }}" class="form-control p-2 @error('name') is-invalid @enderror">
                                                        @error('name') <span class="invalid-feedback"><strong>{{ $mesage }}</strong></span> @enderror
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="email">@lang('back.form-email')</label>
                                                        <input type="email" id="email" name="email" value="{{ $user->email }}" class="form-control p-2 @error('email') is-invalid @enderror">
                                                        @error('email') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                                                    </div>

                                                    <div class="text-center">
                                                        <button type="submit" class="mt-5 btn btn-success btn-block">@lang('back.edit')</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pt-3 pb-3">
                                <label class="text-muted">@lang('back.form-name')</label>
                                <h5 class="text-dark">{{ ucwords($user->name) }}</h5>
                            </div>

                            <div class="pt-3 pb-3">
                                <label class="text-muted">@lang('back.form-email')</label>
                                <h5 class="text-dark">{{ ucwords($user->email) }}</h5>
                            </div>

                            <div class="pt-3 pb-3 change-password">
                                <h5 class="text-dark"> @lang('back.form-password') : <label class="text-muted"> ********** </label></h5>
                                <a href="#" class="text-primary change-pass" data-toggle="modal" data-target="#exampleModal">@lang('back.change-current-password')</a>
                                <!-- Modal -->
                                <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">@lang('back.change-current-password')</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ route('front.changeUserPassword') }}" method="POST">
                                                    @csrf
                                                    <div class="form-group row">
                                                        <label for="currentPassword" class="col-lg-3 col-form-label m-auto">@lang('back.current-password') :</label>
                                                        <div class="col-lg-9">
                                                            <input type="password" name="currentPassword" id="currentPassword" class="form-control p-2 @error('currentPassword') is-invalid @enderror">
                                                            @error('currentPassword') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="newPassword" class="col-lg-3 col-form-label m-auto">@lang('back.new-password')</label>
                                                        <div class="col-lg-9">
                                                            <input type="password" id="newPassword" name="newPassword" class="form-control p-2 @error('newPassword') is-invalid @enderror">
                                                            @error('newPassword') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="newPassword_confirmation" class="col-lg-3 col-form-label m-auto">@lang('back.form-password-confirm')</label>
                                                        <div class="col-lg-9">
                                                            <input type="password" name="newPassword_confirmation" id="newPassword_confirmation" class="form-control p-2">
                                                        </div>
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="submit" class="mt-5 btn btn-primary">@lang('back.edit')</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('#file-input').change(function(){
            readURL(this);
        });
    </script>
@stop
