@extends('layouts.app')

@section('title', trans('back.home'))

@section('style')
    <script type="text/javascript" src="{{ url('public/assets/js/sweetalert.min.js') }}"></script>
    <script>
        function removeImageAjax(id)
        {
            swal({
                title: "Message",
                text: "Are you sure you want to delete this photo",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete)
                {
                    var _token = '{{ csrf_token() }}';
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('front.user.remove.image') }}',
                        data: {id, _token},
                        success: response => {
                            swal(response.message, {icon: "success"});
                            $('#image-row-'+id).fadeOut();
                        },
                        error: err => swal(err, {icon: "warning"}),
                    });
                }
                else
                {
                    swal({
                        title: "Message",
                        text: "Operation Terminated",
                        icon: "success",
                        button: "O.K",
                    });
                }
            });
        }
    </script>
@stop

@section('content')
    @include('includes.nav')
    <section class="check_demo_movie">
        <div class="container">
            <h2 class="wow fadeInDown">Check <span class="main-color"> {{ ucwords($album->name) }} </span> Photos</h2>
            <div class="row">
                <div class="col-md-9">
                    <div class="row" style="margin: 5px;">
                        @forelse($album->images as $photo)
                            <div class="col-md-4" id="image-row-{{$photo->id}}">
                                <div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                                    <div class="card-header">
                                        <img src="{{ $photo->image_url }}" style="cursor: pointer;" onclick="removeImageAjax('{{ $photo->id }}');" class="lazyload" alt="">
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-md-12 alert alert-info text-center">@lang('back.no-value')</div>
                        @endforelse
                    </div>
                </div>
                <div class="col-md-3 contact-form" style="margin-bottom: 22px;">
                    @include('includes.flash')
                    <form action="{{ route('front.user.edit.album', $album->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">@lang('back.form-name')</label>
                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $album->name }}">
                            @error('name')
                            <span class="invalid-feedback"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="type">@lang('back.type')</label>
                            <select name="type" class="form-control @error('name') is-invalid @enderror" id="type">
                                <option value="" disabled selected>Select a value</option>
                                <option {{ $album->type == 'public' ? 'selected' : '' }} value="public">Public</option>
                                <option {{ $album->type == 'private' ? 'selected' : '' }} value="private">Private</option>
                            </select>
                            @error('type') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit1" class="btn btn-success btn-block" value="@lang('back.edit')">
                        </div>
                    </form>

                    <hr>

                    <form action="{{ route('front.user.album.add', $album->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="image">@lang('back.image')</label>
                            <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" id="image">
                            @error('image') <span class="invalid-feedback"><strong>{{ $message }}</strong></span> @enderror
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit2" class="btn btn-success btn-block" value="@lang('back.add')">
                        </div>
                    </form>

                    <hr>

                    <form action="{{ route('front.user.album.del', $album->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="submit" name="submit2" class="btn btn-danger btn-block" value="@lang('back.delete')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
