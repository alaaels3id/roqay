@extends('layouts.app')

@section('title', trans('back.home'))

@section('content')
    @include('includes.nav')
    <section class="check_demo_movie">
        <div class="container">
            <h2 class="wow fadeInDown">Check <span class="main-color"> {{ ucwords($album->name) }} </span> Photos</h2>
            <div class="row">
                @forelse($album->images as $photo)
                    <div class="col-md-4">
                        <div class="card wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                            <div class="card-header">
                                <img src="{{ $photo->image_url }}" class="lazyload" alt="">
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="alert alert-info text-center">@lang('back.no-value')</div>
                @endforelse
            </div>
        </div>
    </section>
@endsection
