<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ direction() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>403</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/front/assets/js/plugins/ui/ripple.min.js') }}"></script>
    <!-- /theme JS files -->
</head>

<body class="login-container">

<!-- Main navbar -->
<div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('admin-panel') }}"><img src="{{ asset('public/front/assets/images/logo_light.png') }}" alt=""></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ auth()->guard('admin')->user()->image_url }}" alt="">
                    <span>{{ ucwords(auth()->guard('admin')->user()->name) }}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('admins.profile') }}"><i class="icon-user-plus"></i> @lang('back.profile')</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-switch2"></i> @lang('back.logout')</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Error title -->
                <div class="text-center content-group">
                    <h1 class="error-title">403</h1>
                    <h5>@lang('back.permissions-error-page')</h5>
                </div>
                <!-- /error title -->

                <!-- Error content -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="text-center">
                            <a href="{{ route('admin-panel') }}" class="btn bg-pink-400"><i class="icon-circle-left2 position-left"></i> @lang('back.home')</a>
                        </div>
                    </div>
                </div>
                <!-- /error wrapper -->

                <!-- Footer -->
                <div class="footer text-muted text-center">&copy;
                    {{ date('Y') }}. <a href="http://opnshop.com"> @lang('back.footer')</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
