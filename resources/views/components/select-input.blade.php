@include('Back.includes.inputs', [
    'errors' => $errors,
    'type'   => 'select',
    'name'   => $name,
    'list'   => count($arr) > 0 ? ['' => trans('back.select-a-value')] + $arr : [0 => trans('back.no-value')],
    'style'  => 'form-control form-data',
    'slug'   => $slug,
])
