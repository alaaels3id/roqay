<div class="checkbox checkbox-switchery checkbox-left switchery-lg" style="font-size: x-large;">
    <label>
        <input
            type="checkbox"
            @isset($role) {{ edit_permissions($role->permissions, $value) }} @endisset
            class="form-control form-data switchery"
            value="{{$value}}"
            @if($value == 'admin-panel') checked @endif
            name="permissions[]"
            id="permissions_{{$key}}">
        @lang('crud.' . $value)
    </label>
</div>
