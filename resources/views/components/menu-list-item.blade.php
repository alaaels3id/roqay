<li class="{{ $type == "" ? setActive('admin-panel/'.$models) : active($models.'.'.$type) }}">
    <a href="{{ url('/admin-panel/'.$models.'/'.$type) }}">@lang($trans)</a>
</li>
