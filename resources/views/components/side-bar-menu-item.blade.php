<li class="{{ setActive('provider-panel/'.$table) }}">
    <a href="#">
        <i class="icon-{{$icon}}"></i>
        <span>@lang('back.'.$table)</span>
    </a>
    <ul>
        <li class="{{ setActive('provider-panel/'.$table.'/create') }}">
            <a href="{{ localeUrl('/provider-panel/'.$table.'/create') }}">@lang('back.add')</a>
        </li>
        <li class="{{ setActive('provider-panel/' . $table) }}">
            <a href="{{ localeUrl('/provider-panel/' . $table) }}">@lang('back.all')</a>
        </li>
        <li class="{{ setActive('provider-panel/'.$table.'/get/trashed') }}">
            <a href="{{ localeUrl(route('providers.'.$table.'.trashed')) }}">@lang('back.trashed')</a>
        </li>
    </ul>
</li>
