<div class="checkbox checkbox-switchery switchery-lg">
    <label>
        @php
            if(isset($model)): $status = $model->status == 1 ? true : false;

            else: $status = true;

            endif;
        @endphp

        {!! Form::checkbox('status',1, $status,['class'=>'form-control switchery form-data','id' => 'status']) !!}

        @lang('back.form-status')
    </label>
</div>
