<!DOCTYPE html>
<html>

<head>
    <title>@yield('title', 'Smart Movies')</title>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="author" content="RoQaY">
    <meta name="robots" content="index, follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Smart Movies website">
    <meta name="keywords" content="Smart Movies">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('public/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('public/front/css/fontawesome.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('public/front/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('public/front/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('public/front/css/style.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('public/front/css/responsive.css') }}">
    @yield('style')
</head>

<body>
<div class="body_wrapper">
    <div class="preloader">
        <div class="preloader-loading">
            <img src="{{ asset('public/front/images/logo-m.png') }}" data-src="{{ asset('public/front/images/logo-m.png') }}" class="lazyload" alt="">
        </div>
    </div>

    <div class="top_nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <ul class="d-flex about-site">
                        <li><a href="#">Questions</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Terms Of Privacy</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul class="d-flex social ">
                        <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                        <li> <a href="#"> <i class="fab fa-twitter"></i> </a></li>
                        <li> <a href="#"> <i class="fab fa-instagram"></i> </a></li>
                        <li> <a href="#"> <i class="fab fa-snapchat-ghost"></i> </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @if(Route::is('sign-in') || Route::is('front.show.register'))
        <div class="logo text-center">
            <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('public/front/images/logo-m.png') }}" data-src="{{ asset('public/front/images/logo-m.png') }}" class="lazyload"></a>
        </div>
    @endif

    @yield('content')

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('public/front/images/logo-m.png') }}" data-src="{{ asset('public/front/images/logo-m.png') }}" class="lazyload"></a>
                    <p> Interact With The Content In An Interesting Educational Experience, Using Studying Means
                        Anywhere & Anytime Directly From your Computer. </p>
                </div>
                <div class="col-md-4">
                    <h5>Links</h5>
                    <div class="links d-flex">
                        <ul>
                            <li> <a href="{{ url('/') }}"> > Home</a></li>
                            @auth
                                <li> <a href="{{ route('front.profile') }}"> > profile</a></li>
                            @else
                                <li> <a href="{{ route('front.register') }}"> > Create Account</a></li>
                                <li> <a href="{{ route('sign-in') }}"> > Login</a></li>
                            @endauth
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5>Contact Us</h5>
                    <div><a href="mailto:info@smartmovie.com"> <i class="fas fa-envelope"></i> info@smartmovie.com</a></div>
                    <form action="">
                        <div class="input-group mb-2">
                            <input type="email" class="form-control" id="inlineFormInputGroup" placeholder=" Your Email ">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <button class="btn btn-gradiant m-0">
                                        <a href="#">Send</a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <ul class="d-flex social ">
                        <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a></li>
                        <li> <a href="#"> <i class="fab fa-twitter"></i> </a></li>
                        <li> <a href="#"> <i class="fab fa-instagram"></i> </a></li>
                        <li> <a href="#"> <i class="fab fa-snapchat-ghost"></i> </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyrights">
            <p>© All Rights reserved to Smart Movies website {{ date('Y') }}</p>
        </div>
    </footer>

    <span class="scroll-top"> <a href="#"><i class="fas fa-chevron-up"></i></a> </span>
</div>
<script src="{{ asset('public/front/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/front/js/popper.min.js') }}"></script>
<script src="{{ asset('public/front/js/lazysizes.min.js') }}"></script>
<script src="{{ asset('public/front/js/fontawesome.min.js') }}"></script>
<script src="{{ asset('public/front/js/all.min.js') }}"></script>
<script src="{{ asset('public/front/js/wow.min.js') }}"></script>
<script src="{{ asset('public/front/js/main.js') }}"></script>
</body>

</html>
