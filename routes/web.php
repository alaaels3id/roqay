<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

# User Login
Route::get('/sign-in', 'UserLoginController@showUserLoginForm')->name('sign-in');
Route::post('/sign-in', 'UserLoginController@userLogin')->name('user.submit.login');

Route::post('/logout', 'UserLoginController@userLogout')->name('user.logout');

# User Routes
Route::name('front.')->group(function () {
    Route::get('/', 'HomeController@index')->name('homepage');
    Route::get('/album/{album}', 'HomeController@showAlbum')->name('show.album');
    Route::get('/register', 'UserController@showUserRegister')->middleware('guest:web')->name('show.register');
    Route::post('/register', 'UserController@register')->name('register');

    Route::group(['middleware' => ['auth', 'CheckUserActive']], function (){
        Route::get('/profile', 'UserController@profile')->name('profile');
        Route::get('/my-albums', 'UserController@myAlbums')->name('my-albums');
        Route::get('/my-albums/{album}/edit', 'UserController@singleAlbum')->name('single.album');
        Route::post('/editAlbum/{album}', 'UserController@editAlbum')->name('user.edit.album');
        Route::post('/addImageToAlbum/{album}', 'UserController@addImageToAlbum')->name('user.album.add');
        Route::post('/delAlbum/{album}', 'UserController@delAlbum')->name('user.album.del');
        Route::post('/createNewAlbum/', 'UserController@createNewAlbum')->name('user.album.create');
        Route::post('/delImageFromAlbum', 'UserController@delImageFromAlbum')->name('user.remove.image');
        Route::get('/my-albums/{album}', 'UserController@getAlbumImages')->name('getAlbumImages');
        Route::post('/updateProfile', 'UserController@updateProfile')->name('updateProfile');
        Route::post('/changeUserPassword', 'UserController@changeUserPassword')->name('changeUserPassword');
    });
});

Route::get('/home', 'HomeController@index')->name('home');
