<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function (){
    # Admin Login
    Route::get('/login', 'AdminLoginController@showAdminLoginForm')->name('admin.login');
    Route::post('/login', 'AdminLoginController@adminLogin')->name('admin.submit.login');
    Route::post('/logout', 'AdminLoginController@adminLogout')->name('admin.logout');
});

# Admin routes
Route::group(['middleware' => ['auth:admin', 'CheckAdminRole', 'CheckAdminStatus']], function () {
    // Admin -> home page
    Route::get('/', 'DashboardController@index')->middleware('SmtpConfigration')->name('admin-panel');

    // admin -> roles Routes
    Route::resource('roles', 'RoleController', ['except' => ['show', 'destroy']]);
    crudRoutes('roles', 'RoleController');

    // admin -> users Routes
    Route::resource('users', 'UserController', ['except' => ['show', 'destroy']]);
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::post('/ajax-delete-user', 'UserController@DeleteUser')->name('ajax-delete-user');
        Route::post('/ajax-change-user-status', 'UserController@ChangeStatus')->name('ajax-change-user-status');
        Route::get('/show/{user}', 'UserController@showUser')->name('show-user');
        Route::get('/export', 'UserController@export')->name('export');
        Route::get('/{id}/show-user-message', 'UserController@showUserMessage')->name('show.message');
        Route::post('/send-user-message', 'UserController@sendUserMessage')->name('send.message');
        Route::get('/get/trashed', 'UserController@trashed')->name('trashed');
        Route::get('/restore/{id}/trashed', 'UserController@restore')->name('restore');
        Route::get('/delete/{id}/trashed', 'UserController@forceDelete')->name('delete');
        Route::post('/send-user-notification', 'UserController@sendUserNotification')->name('send-notification');
    });

    // admin -> Settings Routes
    Route::resource('settings', 'SettingController', ['except' => ['show', 'destroy']]);
    Route::get('settings/exports', 'SettingController@export')->name('settings.export');
    Route::post('settings/update-all', 'SettingController@UpdateAll')->name('settings.update-all');

    // admin -> Admins Routes
    Route::resource('admins', 'AdminController', ['except' => ['show', 'destroy']]);
    Route::group(['prefix' => 'admins', 'as' => 'admins.'], function () {
        Route::get('/admin/profile', 'AdminController@adminProfile')->name('profile');
        Route::post('/admin/profile', 'AdminController@AdminUpdateProfile')->name('admin-profile-update');
        Route::post('/ajax-delete-admin', 'AdminController@DeleteAdmin')->name('ajax-delete-admin');
        Route::post('/ajax-change-admin-status', 'AdminController@ChangeStatus')->name('ajax-change-admin-status');
        Route::get('/show/{admin}', 'AdminController@showAdmin')->name('show-admin');
        Route::get('/{id}/show-admin-message', 'AdminController@showAdminMessage')->name('show.message');
        Route::post('/send-admin-message', 'AdminController@sendAdminMessage')->name('send.message');
        Route::get('/export', 'AdminController@export')->name('export');
        Route::get('/get/trashed', 'AdminController@trashed')->name('trashed');
        Route::get('/restore/{id}/trashed', 'AdminController@restore')->name('restore');
        Route::get('/delete/{id}/trashed', 'AdminController@forceDelete')->name('delete');
    });

    // admin -> albums Routes
    Route::resource('albums', 'AlbumController', ['except' => ['show', 'destroy']]);
    crudRoutes('albums', 'AlbumController', true);

    // admin -> photos Routes
    Route::resource('photos', 'PhotoController', ['except' => ['show', 'destroy']]);
    crudRoutes('photos', 'PhotoController');

    // admin -> Contacts Routes
    Route::group(['prefix' => 'contacts', 'as' => 'contacts.'], function () {
        Route::get('/', 'ContactController@Contacts')->name('index');
        Route::post('/ajax-delete-contact', 'ContactController@DeleteContact')->name('ajax-delete-contact');
        Route::get('/{id}/message-details', 'ContactController@showMessageDetails')->name('message.details');
        Route::get('/{id}/send-message', 'ContactController@sendUserMessage')->name('send-user-message');
        Route::post('/users-send-email', 'ContactController@SendUserReplayMessage')->name('users-send-email');
        Route::get('/export', 'ContactController@ContactsExport')->name('export');
    });
});
