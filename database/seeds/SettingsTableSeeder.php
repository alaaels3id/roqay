<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        $root = request()->root();

        DB::table('settings')->insert([
            self::setRow('map_api', 'Map', 'API', 0, 'AIzaSyBGZZPZWVePr6Ba7SNCwwTSn1YAsV6Smok'),
            self::setRow('notification_key', 'Notification Key', 'API', 0),

            self::setRow('smtp', 'driver', 'SMTP', 0, 'smtp'),
            self::setRow('smtp_port', 'SMTP port', 'SMTP', 0, '2525'),
            self::setRow('smtp_host', 'SMTP host', 'SMTP', 0, 'smtp.mailtrap.io'),
            self::setRow('smtp_sender_name', 'SMTP sender name', 'SMTP', 0, config('app.name')),
            self::setRow('smtp_sender_email', 'SMTP sender email', 'SMTP', 0, 'info@site.com'),
            self::setRow('smtp_encryption', 'SMTP encryption', 'SMTP', 0, 'tls'),
            self::setRow('smtp_username', 'SMTP username', 'SMTP', 0, 'efc9853ca1a258'),
            self::setRow('smtp_password', 'SMTP password', 'SMTP', 0, '55b6b6f0114f15'),

            self::setRow('sms_number', 'SMS Number', 'SMS', ''),
            self::setRow('sms_password', 'SMS Password', 'SMS', ''),
            self::setRow('sms_sender_name', 'SMS Sender Name', 'SMS', ''),

            self::setRow('about_app_ar', 'about app ar', 'APP', 4, '<section class="info">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-12 m-auto">
                        <div class="wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <p>هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                            , حيث يمكنك ان تولد مثل هذا النص او العديد من النصوص الأخرى اضافة الى زيادة عدد الحروف التى يولدها التطبيق
                                اذا كنت تحتاج الى عدد كبير من الفقرات يتيح لك مولد النص العربى
                                هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                        </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <img src="'.$root.'/public/front/imgs/Group 1211.png" class="img-fluid wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".5s">
                    </div>
                </div>
                <div class="mt-5 wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".5s">
                    <p>هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                        , حيث يمكنك ان تولد مثل هذا النص او العديد من النصوص الأخرى اضافة الى زيادة عدد الحروف التى يولدها التطبيق
                        اذا كنت تحتاج الى عدد كبير من الفقرات يتيح لك مولد النص العربى
                        هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                        هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                        , حيث يمكنك ان تولد مثل هذا النص او العديد من النصوص الأخرى اضافة الى زيادة عدد الحروف التى يولدها التطبيق
                        اذا كنت تحتاج الى عدد كبير من الفقرات يتيح لك مولد النص العربى
                        هذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                        ذا النص هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي
                         حيث يمكنك ان تولد مثل هذا النص او العديد من النصوص الأخرى اضافة الى زيادة عدد الحروف التى يولدها التطبيق
                         اذا كنت تحتاج الى عدد كبير من الفقرات يتيح لك مولد النص العربى
                    </p>
                </div>
            </div>
        </section>'),
            self::setRow('about_app_en', 'about app en', 'APP', 4, '<section class="info">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-12 m-auto">
                        <div class="wow fadeInRight" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <img src="'.$root.'/public/front/imgs/Group 1211.png" class="img-fluid wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".5s">
                    </div>
                </div>
                <div class="mt-5 wow fadeIn" data-wow-duration="1.5s" data-wow-delay=".5s">
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                </div>
            </div>
        </section>'),

            self::setRow('contact_facebook', 'صفحة الفيسبوك', 'CONTACTS', 0, 'https://facebook.com'),
            self::setRow('contact_twitter', 'صفحة تويتر', 'CONTACTS', 0, 'https://twitter.com'),
            self::setRow('contact_instgram', 'صفحة انستجرام', 'CONTACTS', 0, 'https://instgram.com'),
            self::setRow('contact_phone', 'أرقام التواصل', 'CONTACTS', 0, '(996 10486777+) - (996 10486999+)'),
            self::setRow('contact_email', 'البريد الالكتروني', 'CONTACTS', 0, 'info@site.com'),
            self::setRow('contact_site_name_ar', 'إسم الموقع باللغة العربية', 'CONTACTS', 0, 'منشئات'),
            self::setRow('contact_site_name_en', 'إسم الموقع باللغة الانجليزية', 'CONTACTS', 0, 'منشئات'),
            self::setRow('contact_site_address', 'العنوان', 'CONTACTS', 0, 'السعودية , الرياض , شارع القصيم'),
            self::setRow('contact_site_description_ar', 'وصف الموقع بالعربية', 'CONTACTS', 0, 'هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي , حيث يمكنك ان تولد مثل هذا النص او العديد من النصوص الأخرى اضافة الى زيادة عدد الحروف التى يولدها التطبيق اذا كنت تحتاج الى عدد كبير من الفقرات يتيح لك مولد النص العربى'),
            self::setRow('contact_site_description_en', 'وصف الموقع بالانجليزية', 'CONTACTS', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'),
            self::setRow('contact_site_location', 'مكان الشركة علي الخريطة', 'CONTACTS', 3, '31.033184321756003,31.38131713867239'),
        ]);
    }

    private static function setRow($key, $name, $type, $input, $value = null)
    {
        return [
            'key'        => $key,
            'name'       => $name,
            'type'       => $type,
            'input'      => $input,
            'value'      => $value,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
