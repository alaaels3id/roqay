<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert(['created_at' => now(), 'updated_at' => now()]);

        DB::table('role_translations')->insert([
            ['role_id' => 1, 'locale' => 'ar', 'name' => 'مدير عام'],
            ['role_id' => 1, 'locale' => 'en', 'name' => 'Super admin'],
        ]);

        DB::table('permissions')->insert([
            'role_id' => 1,
            'permission' => '*',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
