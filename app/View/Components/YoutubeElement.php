<?php

namespace App\View\Components;

use Illuminate\View\Component;

class YoutubeElement extends Component
{
    public $headTitle;
    public $source;

    public function __construct($headTitle, $source)
    {
        $this->headTitle = $headTitle;
        $this->source = $source;
    }

    public function render()
    {
        return view('components.youtube-element');
    }
}
