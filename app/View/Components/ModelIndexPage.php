<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelIndexPage extends Component
{
    public $model;
    public $modelType;

    public function __construct($model, $modelType)
    {
        $this->model = $model;
        $this->modelType = $modelType;
    }

    public function render()
    {
        return view('components.model-index-page');
    }
}
