<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectInput extends Component
{
    public $arr;
    public $name;
    public $slug;
    public $errors;

    public function __construct($arr, $name, $slug, $errors)
    {
        $this->arr    = $arr;
        $this->name   = $name;
        $this->slug   = $slug;
        $this->errors = $errors;
    }

    public function render()
    {
        return view('components.select-input');
    }
}
