<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormInputs extends Component
{
    public $type;
    public $name;
    public $slug;
    public $errors;

    public function __construct($type, $name, $slug, $errors)
    {
        $this->type = $type;
        $this->name = $name;
        $this->slug = $slug;
        $this->errors = $errors;
    }

    public function render()
    {
        return view('components.form-inputs');
    }
}
