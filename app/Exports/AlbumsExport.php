<?php

namespace App\Exports;

use App\Models\Album;
use Maatwebsite\Excel\Concerns\FromCollection;

class AlbumsExport implements FromCollection
{
    public function collection()
    {
        return Album::select(['id', 'name', 'user_id', 'created_at'])->get();
    }
}
