<?php

namespace App\Exports;

use App\Models\Photo;
use Maatwebsite\Excel\Concerns\FromCollection;

class PhotosExport implements FromCollection
{
    public function collection()
    {
        return Photo::select(['id', 'album_id', 'user_id', 'created_at'])->get();
    }
}
