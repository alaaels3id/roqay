<?php


namespace App\Crud;


class Api
{
    public static function facadeResolve($method)
    {
        return app()->make($method);
    }

    public static function __callStatic($method, $arguments)
    {
        return (Self::facadeResolve('Api'))->$method(...$arguments);
    }
}
