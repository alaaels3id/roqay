<?php

namespace App\Crud;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ApiProcess
{
    /**
     * @param $model
     * @param $resource
     * @param $request
     * @param $guard
     * @return JsonResponse
     */
    public function apiLogin($model, $resource, $request, $guard = 'api')
    {
        $user = $model::where(['phone' => $request->phone])->first();

        if($user && !$user->active) return apiResponseWarning('عفوا هذا الحساب غير نشط حاليا برجاء تفعيل حسابك اولا');

        if (!$token = Auth::guard($guard)->attempt(['phone' => $request->phone, 'password' => $request->password, 'status' => 1]))
        {
            return apiResponse(null,'fails',null, trans('auth.failed'), 203);
        }

        if($request->has('fcm_token')) auth()->guard($guard)->user()->fcm->update(['fcm' => $request->fcm_token]);

        auth()->guard($guard)->user()->token->update(['jwt' => $token, 'ip' => $request->ip()]);

        return apiResponse(new $resource(auth()->guard($guard)->user()),'success');
    }

    /**
     * @param $model
     * @param $resource
     * @param $request
     * @param array $other
     * @return JsonResponse
     */
    public function apiRegister($model, $resource, $request, $other = [])
    {
        $data = $request->all();

        if(!empty($other)) $data = $data + $other;

        $model_name = Str::of(last(explode('\\', $model)))->lower();

        try
        {
            if($request->hasFile('image')) $data['image'] = uploaded($request->image, $model_name);

            else $data = Arr::except($data, ['image']);

            $created = $model::create($data);

            if($request->hasFile('image')) $created->image()->create(['image' => $data['image']]);

            return apiResponse(new $resource($created), 'success');
        }
        catch(Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }

    /**
     * @param $model
     * @param $resource
     * @param $request
     * @param bool $imageModel
     * @return JsonResponse
     */
    public function apiStore($model, $resource, $request, $imageModel = false)
    {
        $data = !is_array($request) ? $request->all() : $request;

        $modelName = Str::lower(last(explode('\\', $model)));

        $image = null;

        try
        {
            if (request()->hasFile('image')) $image = uploaded($data['image'], $modelName);

            else $data = Arr::except($data, ['image']);

            $created = $model::create(Arr::except($data, ['_token', 'password_confirmation', 'image']));

            if(request()->hasFile('image') && $imageModel) $created->image()->create(['image' => $image]);

            return apiResponse(new $resource($created),'success');
        }
        catch (Exception $e)
        {
            return apiResponseFails($e->getMessage());
        }
    }
}
