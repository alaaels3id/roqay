<?php

namespace App\Models;

use App\Crud\Crud;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function album()
    {
        return $this->belongsTo(Album::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getImageUrlAttribute()
    {
        return getDefaultImage($this, 'photos');
    }

    public static function changeStatus($request)
    {
        return Crud::setStatus(self::class, $request);
    }

    public static function createPhoto($request)
    {
        return Crud::store(self::class, $request, true);
    }

    public static function updatePhoto($request, $currentPhoto)
    {
        return Crud::update(self::class, $request, $currentPhoto, true);
    }

    public static function deletePhoto($request)
    {
        return Crud::delete(self::class, $request->id);
    }
}
