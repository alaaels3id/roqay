<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['id'];

    public function notificationable()
    {
        return $this->morphTo();
    }
}
