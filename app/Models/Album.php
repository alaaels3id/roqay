<?php

namespace App\Models;

use App\Crud\Crud;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function getImageUrlAttribute()
    {
        return ($this->images->count() > 0) ? url('public/uploaded/photos/' . $this->images[0]->image->image) : defaultImage();
    }

    public function images()
    {
        return $this->hasMany(Photo::class, 'album_id', 'id');
    }

    public static function types()
    {
        return ['public' => trans('back.public'), 'private' => trans('back.private')];
    }

    public static function changeStatus($request)
    {
        return Crud::setStatus(self::class, $request);
    }

    public static function createAlbum($request)
    {
        return Crud::store(self::class, $request);
    }

    public static function updateAlbum($request, $currentAlbum)
    {
        return Crud::update(self::class, $request, $currentAlbum);
    }

    public static function deleteAlbum($request)
    {
        return Crud::delete(self::class, $request->id);
    }

    public static function getInSelectForm()
    {
        return Crud::getModelsInSelectedForm(self::class,'name');
    }
}
