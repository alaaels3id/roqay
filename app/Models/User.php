<?php

namespace App\Models;

use App\Crud\Crud;
use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    protected $guarded = ['id'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['email_verified_at' => 'datetime', 'status' => 'boolean'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getImageUrlAttribute()
    {
        return getDefaultImage($this, 'users');
    }

    public function token()
    {
        return $this->morphOne(Token::class, 'tokenable');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function fcm()
    {
        return $this->morphOne(Fcm::class,'fcmable');
    }

    public function albums()
    {
        return $this->hasMany(Album::class,'user_id', 'id');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function changeStatus($request)
    {
        return Crud::setStatus(self::class, $request);
    }

    public static function createUser($request)
    {
        return Crud::store(self::class, $request, true);
    }

    public static function updateUser($request, $currentUser)
    {
        return Crud::update(self::class, $request, $currentUser, true);
    }

    public static function deleteUser($request)
    {
        return Crud::delete(self::class, $request->id);
    }

    public static function sendUserMessage($request)
    {
        return Crud::sendUserMessage($request);
    }

    public static function register($request)
    {
        try
        {
            Auth::login(User::create([
                "name"     => $request->name,
                "email"    => $request->email,
                "password" => $request->password,
            ]));

            return redirect('/');
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }

    public static function getInSelectForm()
    {
        return Crud::getModelsInSelectedForm(self::class,'name');
    }
}
