<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fcm extends Model
{
    protected $guarded = ['id'];

    public function fcmable()
    {
        return $this->morphTo();
    }

    public static function createFcm($createdModel, $type)
    {
        return Fcm::create(['fcmable_id' => $createdModel->id, 'fcmable_type' => $type, 'fcm' => request()->fcm_token ?? null]);
    }

    public static function updateFcm($model)
    {
        return $model->fcm()->update(['fcm' => request()->fcm_token ?? null]);
    }
}
