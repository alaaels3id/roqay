<?php

namespace App\Models;

use App\Crud\Crud;
use Astrotomic\Translatable\Translatable;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    use Translatable, SoftDeletes;

    public $translationModel = RoleTranslation::class;

    public $translationForeignKey = 'role_id';

    public $translatedAttributes = ['name'];

    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'role_id', 'id');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean)$value;
    }

    public static function createRole($request)
    {
        DB::beginTransaction();
        try {
            $roleData = $request->all();

            $formTranslatedAttrs = arr()->only($roleData, array_keys(config('sitelangs.locales')));

            $roleData = arr()->except($roleData, array_keys(config('sitelangs.locales')));

            $role = new Role(arr()->except($roleData, ['_token', 'permissions']));

            save_translated_attrs($role, $formTranslatedAttrs);

            $role->save();

            $permissions = [];

            foreach ($request->permissions as $key => $permission)
            {
                if ($permission == null) continue;

                $permissions[$key]['role_id'] = $role->id;

                $permissions[$key]['permission'] = $permission;

                $permissions[$key]['created_at'] = now();

                $permissions[$key]['updated_at'] = now();
            }


            DB::table('permissions')->insert($permissions);

            DB::commit();

            return response()->json(['requestStatus' => true, 'message' => translated('add', 'permission')]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['requestStatus' => false, 'message' => $e->getMessage()]);
        }
    }

    public static function updateRole($request, $currentRole)
    {
        DB::beginTransaction();
        try {
            $roleData = $request->except('permissions');

            $permissions = [];

            foreach ($request->permissions as $key => $permission)
            {
                if ($permission == null) continue;

                $permissions[$key]['role_id'] = $currentRole->id;
                $permissions[$key]['created_at'] = now();
                $permissions[$key]['updated_at'] = now();
                $permissions[$key]['permission'] = $permission;
            }

            $formTranslatedAttrs = arr()->only($roleData, array_keys(config('sitelangs.locales')));

            $roleData = arr()->except($roleData, array_keys(config('sitelangs.locales')));

            save_translated_attrs($currentRole, $formTranslatedAttrs, $roleData);

            $currentRole->save();

            DB::table('permissions')->where('role_id', $currentRole->id)->delete();

            DB::table('permissions')->insert($permissions);

            DB::commit();

            return response()->json(['requestStatus' => true, 'message' => translated('edit', 'permission')], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['requestStatus' => false, 'message' => $e->getMessage()]);
        }
    }

    public static function deleteRole($request)
    {
        return Crud::delete(Role::class, $request->id);
    }

    public static function changeStatus($request)
    {
        return Crud::setStatus(Role::class, $request);
    }

    public static function getInSelectForm($with_main = null, $with_null = null, $exceptedIds = [])
    {
        $roles = [];

        $with_null == null ? $roles = $roles : $roles = $roles + ['' => ''];
        $with_main == null ? $roles = $roles : $roles = $roles + ['0' => 'Main'];

        $rolesDB = Role::whereNotIn('id', $exceptedIds)->where('id', '!=', 1)->get();

        foreach ($rolesDB as $role) {
            $roles[$role->id] = ucwords($role->name);
        }

        return $roles;
    }
}
