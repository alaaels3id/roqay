<?php

namespace App\Models;

use App\Crud\Crud;
use App\Events\SendUserAnEmailEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Exception;

class Contact extends Model
{
    protected $guarded = ['id'];

    public function getDiffForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getLastUpdateAttribute()
    {
        return $this->updated_at->diffForHumans();
    }

    public static function deleteMessage($request)
    {
        return Crud::delete(Contact::class, $request->id);
    }

    public static function createContactUsMessage($request)
    {
        try
        {
            Contact::create(['name' => $request->name, 'email' => $request->email, 'message' => $request->message]);

            return back()->with('success', trans('api.request-done-successfully'));
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }

    public static function sendUserReplayMessage(Request $request)
    {
        try
        {
            event(new SendUserAnEmailEvent($request->message, $request->email));
            return back()->with('success', 'تم ارسال الرسالة بنجاح');
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }
}
