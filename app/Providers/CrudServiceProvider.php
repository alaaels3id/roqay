<?php

namespace App\Providers;

use App\Crud\CrudProcess;
use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Crud', function ($app) {
            return new CrudProcess();
        });
    }

    public function boot()
    {
        //
    }
}
