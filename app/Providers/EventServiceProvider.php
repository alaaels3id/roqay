<?php

namespace App\Providers;

use App\Events\SendAdminMailEvent;
use App\Events\SendResetCodeMailEvent;
use App\Events\SendUserAnEmailEvent;
use App\Listeners\SendAdminMailListener;
use App\Listeners\SendResetCodeMailListener;
use App\Listeners\SendUserAnEmailListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class             => [SendEmailVerificationNotification::class],
        SendUserAnEmailEvent::class   => [SendUserAnEmailListener::class],
        SendAdminMailEvent::class     => [SendAdminMailListener::class],
        SendResetCodeMailEvent::class => [SendResetCodeMailListener::class],
    ];

    public function boot()
    {
        parent::boot();
    }
}
