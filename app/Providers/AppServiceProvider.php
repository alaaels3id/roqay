<?php

namespace App\Providers;

use App\Crud\ApiProcess;
use App\Models\Section;
use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        Schema::enableForeignKeyConstraints();

        View::composer('*', function ($view) { return $view->with('auth', auth()->user()); });
    }

    public function boot()
    {
        View::composer('*', function ($view) {return $view->with('getAllRoutes', getAllRoutes());});

        Blade::if('hasPermission', function ($route) { return permission_route_checker($route); });

        $this->app->singleton('Api', function ($app) { return new ApiProcess(); });

        User::observe(UserObserver::class);
    }
}
