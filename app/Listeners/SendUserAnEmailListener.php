<?php

namespace App\Listeners;

use App\Events\SendUserAnEmailEvent;
use App\Mail\SendUserEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendUserAnEmailListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(SendUserAnEmailEvent $event)
    {
        Mail::to($event->email)->queue(new SendUserEmail($event->message));
    }
}
