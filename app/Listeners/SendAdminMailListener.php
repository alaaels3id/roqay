<?php

namespace App\Listeners;

use App\Events\SendAdminMailEvent;
use App\Mail\SendAdminMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendAdminMailListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(SendAdminMailEvent $event)
    {
        Mail::to($event->email)->send(new SendAdminMail($event->message));
    }
}
