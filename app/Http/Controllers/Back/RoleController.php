<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\BASE_CONTROLLER;
use App\Http\Requests;
use App\Models\Role;
use App\Exports\RolesExport;
use Illuminate\Http\Request;

class RoleController extends BASE_CONTROLLER
{
    public function __construct()
    {
        parent::__construct('role', Role::class, new RolesExport);
    }

    public function index()
    {
        return view('Back.Roles.index', ['roles' => Role::orderBy('id', 'desc')->where('id', '!=', 1)->get()]);
    }

    public function store(Requests\Back\CreateRoleRequest $request)
    {
        return Role::createRole($request);
    }

    public function update(Requests\Back\EditRoleRequest $request, $id)
    {
        return Role::updateRole($request, Role::find($id));
    }

    public function DeleteRole(Request $request)
    {
        return Role::deleteRole($request);
    }
}
