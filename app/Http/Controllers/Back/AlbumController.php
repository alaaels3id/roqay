<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\BASE_CONTROLLER;
use App\Http\Requests;
use App\Models\Album;
use App\Models\User;
use App\Exports\AlbumsExport;
use Illuminate\Http\Request;

class AlbumController extends BASE_CONTROLLER
{
    public function __construct()
    {
        parent::__construct('album', Album::class, new AlbumsExport);
    }

    public function create()
    {
        return view('Back.Crud.create', ['model' => 'album', 'users' => User::getInSelectForm(), 'types' => Album::types()]);
    }

    public function showAlbum(Album $album)
    {
        return view('Back.Albums.show', compact('album'));
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', ['currentModel' => Album::findOrFail($id), 'model' => 'album', 'users' => User::getInSelectForm(), 'types' => Album::types()]);
    }

    public function store(Requests\Back\CreateAlbumRequest $request)
    {
        return Album::createAlbum($request);
    }

    public function update(Requests\Back\EditAlbumRequest $request, $id)
    {
        return Album::updateAlbum($request, Album::find($id));
    }

    public function DeleteAlbum(Request $request)
    {
        return Album::deleteAlbum($request);
    }
}
