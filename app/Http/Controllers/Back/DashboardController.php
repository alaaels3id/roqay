<?php

namespace App\Http\Controllers\Back;

use App\Models\Agency;
use App\Models\BankTransfer;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();

        return view('Back.index', [
            'latest_users' => $users->take(4),
            'days'         => getCollectionBerDayCount($users),
//            'months'       => getCollectionBerMonthCount(BankTransfer::whereStatus(1)->groupBy(['package_id', 'created_at'])->selectRaw('sum(price) as sum, created_at')->get()),
            'users'        => $users
        ]);
    }
}
