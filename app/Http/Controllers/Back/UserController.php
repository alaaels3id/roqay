<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\BASE_CONTROLLER;
use App\Http\Requests;
use App\Models\City;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use App\Exports\UsersExport;

class UserController extends BASE_CONTROLLER
{
    public function __construct()
    {
        parent::__construct('user', User::class, new UsersExport);
    }

    public function showUserMessage($id)
    {
        if (!$user = User::find($id)) return response()->json(['requestStatus' => false, 'message' => trans('responseMessages.product-not-exist')]);

        return view('Back.Users.sendUserMessageModal', compact('user'))->render();
    }

    public function forceDelete($id)
    {
        modelForceDelete(User::class, $id, true);

        return back()->with('success', translated('remove','user'));
    }

    public function sendUserMessage(Request $request)
    {
        return User::sendUserMessage($request);
    }

    public function showUser(User $user)
    {
        return view('Back.Users.show', compact('user'));
    }

    public function store(Requests\Back\CreateUserRequest $request)
    {
        return User::createUser($request);
    }

    public function update(Requests\Back\EditUserRequest $request, $id)
    {
        return User::updateUser($request, User::find($id));
    }

    public function DeleteUser(Request $request)
    {
        return User::deleteUser($request);
    }
}
