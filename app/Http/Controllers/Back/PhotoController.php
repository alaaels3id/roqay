<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\BASE_CONTROLLER;
use App\Http\Requests;
use App\Models\Album;
use App\Models\Photo;
use App\Models\User;
use App\Exports\PhotosExport;
use Illuminate\Http\Request;

class PhotoController extends BASE_CONTROLLER
{
    public function __construct()
    {
        parent::__construct('photo', Photo::class, new PhotosExport);
    }

    public function create()
    {
        return view('Back.Crud.create', [
            'model'  => 'photo',
            'albums' => Album::getInSelectForm(),
        ]);
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', [
            'currentModel' => Photo::findOrFail($id),
            'model'        => 'photo',
            'albums'       => Album::getInSelectForm(),
        ]);
    }

    public function store(Requests\Back\CreatePhotoRequest $request)
    {
        return Photo::createPhoto($request);
    }

    public function update(Requests\Back\EditPhotoRequest $request, $id)
    {
        return Photo::updatePhoto($request, Photo::find($id));
    }

    public function DeletePhoto(Request $request)
    {
        return Photo::deletePhoto($request);
    }
}
