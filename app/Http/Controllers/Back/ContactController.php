<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\ContactsExport;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{
    public function Contacts()
    {
        return view('Back.Contacts.contacts', ['contacts' => Contact::all()]);
    }

    public function showMessageDetails($id)
    {
        if (!$message = Contact::find($id)) return response()->json(['requestStatus' => false, 'message' => trans('responseMessages.product-not-exist')]);

        return view('Back.Contacts.messageDetailsModal', compact('message'))->render();
    }

    public function sendUserMessage($id)
    {
        if (!$message = Contact::find($id)) return response()->json(['requestStatus' => false, 'message' => trans('back.message-not-exists')]);

        return view('Back.Contacts.sendUserMessageModal', compact('message'));
    }

    public function SendUserReplayMessage(Request $request)
    {
        return Contact::sendUserReplayMessage($request);
    }

    public function ContactsExport(Request $request)
    {
        return Excel::download(new ContactsExport, 'contacts.xlsx');
    }

    public function DeleteContact(Request $request)
    {
        return Contact::deleteMessage($request);
    }
}
