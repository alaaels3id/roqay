<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\Front\buyProjectRequest;
use App\Http\Requests\Front\changeUserPasswordRequest;
use App\Http\Requests\Front\orderServiceRequest;
use App\Http\Requests\Front\setUserTenderRequest;
use App\Http\Requests\Front\updateProfileRequest;
use App\Http\Requests\Front\userRegisterRequest;
use App\Models\Agency;
use App\Models\Album;
use App\Models\BuyProject;
use App\Models\Category;
use App\Models\City;
use App\Models\OrderService;
use App\Models\Page;
use App\Models\Photo;
use App\Models\Project;
use App\Models\Service;
use App\Models\Tender;
use App\Models\User;
use App\Models\UserTender;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function showUserRegister()
    {
        return view('auth.pages.userRegister');
    }

    public function profile()
    {
        return view('Front.pages.profile', ['user' => auth()->user()]);
    }

    public function myAlbums()
    {
        return view('Front.pages.myAlbums', ['user' => auth()->user()]);
    }

    public function updateProfile(updateProfileRequest $request)
    {
        $user = auth()->user();

        $image = isset($user->image) ? $user->image->image : '';

        if($request->hasFile('image'))
        {
            File::delete('public/uploaded/users/' . $image);

            if($image == "") $user->image()->create(['image' => uploaded($request->image, 'user')]);

            else $user->image()->update(['image' => uploaded($request->image, 'user')]);
        }

        $user->update($request->except(['_token', 'submit', 'image']));

        return back()->with('success', trans('back.data-edited-successfully'));
    }

    public function changeUserPassword(changeUserPasswordRequest $request)
    {
        if(!Hash::check($request->currentPassword, auth()->user()->password)) return back()->with('danger', trans('back.old-password-incorrect'));

        auth()->user()->update(['password' => $request->newPassword]);

        return back()->with('success', trans('back.change-password-success'));
    }

    public function register(userRegisterRequest $request)
    {
        return User::register($request);
    }

    public function editAlbum(Request $request, Album $album)
    {
        $request->validate(['name' => 'required|string', 'type' => 'required|in:public,private']);

        $album->update(['name' => $request->name, 'type' => $request->type]);

        return back()->with('success', 'Done successfully');
    }

    public function delAlbum(Request $request, Album $album)
    {
        $album->delete();

        return redirect('/my-albums')->with('success', 'Done successfully');
    }

    public function singleAlbum(Album $album)
    {
        return view('Front.pages.singleAlbum', compact('album'));
    }

    public function addImageToAlbum(Request $request, Album $album)
    {
        $request->validate(['image' => 'required|mimes:png,jpg,jpeg']);

        $photo = Photo::create(['album_id' => $album->id]);

        $photo->image()->create(['image' => uploaded($request->image, 'photo')]);

        return back()->with('success', 'Done successfully');
    }

    public function delImageFromAlbum(Request $request)
    {
        $photo = Photo::find($request->id);

        $res = $photo->delete();

        $arr['mess'] = $res ? 'Done successfully' : 'Operation fails';

        $arr['code'] = $res ? 200 : 500;

        return response()->json(['message' => $arr['mess']], $arr['code']);
    }

    public function createNewAlbum(Request $request)
    {
        $request->validate(['type' => 'required|in:public,private', 'name' => 'required|string']);

        Album::create([
            'user_id' => auth()->id(),
            'name' => $request->name,
            'type' => $request->type,
        ]);

        return back()->with('success', 'Done successfully');
    }
}
