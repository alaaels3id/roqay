<?php

namespace App\Http\Controllers\Front;

use App\Models\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('Front.index', ['albums' => Album::where('type', 'public')->where('status', 1)->get()]);
    }

    public function showAlbum(Album $album)
    {
        return view('Front.pages.showAlbum', compact('album'));
    }
}
