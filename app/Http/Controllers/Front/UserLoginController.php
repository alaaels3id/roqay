<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\Auth\UserLoginRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class UserLoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:web')->except('userLogout');
    }

    public function showUserLoginForm()
    {
        return view('auth.pages.userLogin');
    }

    public function userLogin(UserLoginRequest $request)
    {
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])) return back()->withInput($request->only('email'))->with('danger', 'بيانات الاعتماد لا تتطابق مع التي لدينا');

        if(!auth()->user()->status)
        {
            auth()->logout();

            return back()->with('warning', 'عفوا هذا الحساب معطل حاليا من الادارة');
        }

        return redirect()->intended(url('/'));
    }

    public function userLogout()
    {
        Auth::guard('web')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();

        return redirect('/');
    }
}
