<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Tymon\JWTAuth\Facades\JWTAuth;

class PARENT_API extends Controller
{
    public $lang;
    public $user;

    function __construct(Request $request)
    {
        $this->Set_Request_Language($request);

        if ($request->header('Authorization') && JWTAuth::parseToken())
        {
            try
            {
                // JWTAuth::parseToken()->authenticate()
                $this->user = JWTAuth::parseToken()->toUser();
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }

    function Set_Request_Language($request)
    {
        $this->lang = ($request->header('lang')) ? (($request->header('lang') == "ar") ? "ar" : "en") : "en";

        App::setLocale($this->lang);

        Carbon::setLocale($this->lang);
    }
}
