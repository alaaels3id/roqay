<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'permissions.0' => 'required',
        ];

        foreach (config('sitelangs.locales') as $lang => $name) {
            $rules[$lang.'.name'] = 'required|string';
        }

        return $rules;
    }
}
