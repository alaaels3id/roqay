<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class EditAlbumRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'user_id' => 'required|numeric|exists:users,id',
            'type' => 'required|string|in:public,private'
        ];
    }
}
