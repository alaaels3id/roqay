<?php

namespace App\Http\Requests\Back;

use App\Rules\EmailFormatChecker;
use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|string',
            'email'    => ['required','email', new EmailFormatChecker(), 'unique:users,email,'.$this->user],
            'image'    => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];
    }
}
