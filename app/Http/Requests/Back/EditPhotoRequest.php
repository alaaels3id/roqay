<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class EditPhotoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'image' => 'nullable|mimes:png,jpg,jpeg',
            'album_id' => 'required|numeric|exists:albums,id',
        ];
    }
}
