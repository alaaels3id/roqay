<?php

namespace App\Http\Requests\Back;

use App\Rules\EmailFormatChecker;
use Illuminate\Foundation\Http\FormRequest;

class EditAdminRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|string',
            'role_id'  => 'required|numeric|exists:roles,id',
            'email'    => ['required','email', new EmailFormatChecker(), 'unique:admins,email,'.$this->admin],
            'image'    => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
            'password' => 'nullable|string|confirmed|min:6',
        ];
    }
}
