<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminRole
{
    public function handle($request, Closure $next)
    {
        if ($request->ajax()) {
            if (!permission_checker(auth()->guard('admin')->user())) return response()->json(['requestStatus' => false, 'message' => 'عفوا انت غير مصرح لك بعمل هذا'], 401);
        }

        if (!permission_checker(auth()->guard('admin')->user())) return abort(403);

        return $next($request);
    }
}
