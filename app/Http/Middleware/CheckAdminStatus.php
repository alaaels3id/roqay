<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminStatus
{
    public function handle($request, Closure $next)
    {
        if(!$request->user()->status)
        {
            Auth::guard('admin')->logout();

            return redirect('/admin-panel/login')->with('accountLocked', 'عفوا تم ايقاف تفعيل هذا الحساب من قبل الادارة يرجي التواصل لحل المشكلة');
        }
        return $next($request);
    }
}
