<?php

namespace App\Mail;

use App\Models\Agency;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAgencyResetCode extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $agency;
    public $message;

    public function __construct(Agency $agency, $message)
    {
        $this->agency = $agency;
        $this->message = $message;
    }

    public function build()
    {
        return $this->markdown('emails.sendAgencyResetCode');
    }
}
