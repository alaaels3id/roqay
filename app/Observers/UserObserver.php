<?php

namespace App\Observers;

use App\Models\Fcm;
use App\Models\Token;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserObserver
{
    public function created(User $user)
    {
        // create the user token;
        Token::createToken($user, Auth::guard('api')->fromUser($user), User::class);

        // create the user firebase token
        Fcm::createFcm($user, User::class);
    }
}
